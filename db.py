import datetime
from peewee import SqliteDatabase, Model, CharField, BooleanField, ForeignKeyField, IntegerField, DateTimeField

db = SqliteDatabase('bot.db')


class User(Model):
    uid = CharField()

    class Meta:
        database = db


class TestAttempt(Model):
    user = ForeignKeyField(User, related_name='test_attempts')
    finished = BooleanField(default=False)
    created_at = DateTimeField(default=datetime.datetime.now)

    class Meta:
        database = db


class Answer(Model):
    test_attempt = ForeignKeyField(TestAttempt, related_name='answers')
    question_num = IntegerField()
    value = IntegerField()

    class Meta:
        database = db
