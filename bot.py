import json
import random

import requests
import vk
from peewee import fn

from db import User, db, TestAttempt, Answer
from texts import TEXTS


class Bot(object):
    def __init__(self, token):
        self.session = vk.Session(access_token=token)
        self.api = vk.API(self.session, v='5.53')
        self.image_mapping = {}
        self.preload_chat_images()

    def preload_chat_images(self):
        resp = self.api.photos.getMessagesUploadServer()
        upload_url = resp.get('upload_url')
        for idx in [1, 2, 3, 4]:
            files = {'photo': open('images/img{}.png'.format(idx), 'rb')}
            req = requests.post(upload_url, files=files)
            r = json.loads(req.text)
            photo_resp = self.api.photos.saveMessagesPhoto(photo=r['photo'], server=r['server'], hash=r['hash'])
            self.image_mapping['img{}'.format(idx)] = 'photo{}_{}'.format(photo_resp[0]['owner_id'],
                                                                          photo_resp[0]['id'])

    def process_message(self, message):
        self.api.messages.markAsRead(message_ids=[message['id'], ], peer_id=message['user_id'])
        user_id = str(message['user_id'])

        try:
            user = User.get(User.uid == message['user_id'])
        except Exception:
            user = User.create(uid=message['user_id'])

        last_attempt = None
        last_answer = None
        try:
            last_attempt = (TestAttempt
                            .select()
                            .join(User)
                            .where(User.uid == message['user_id'])
                            .order_by(TestAttempt.created_at.desc())
                            .get())
            last_answer = (Answer
                           .select()
                           .where(Answer.test_attempt == last_attempt)
                           .order_by(Answer.question_num.desc())
                           .get())
        except Exception:
            pass

        body = message['body']
        if last_attempt and not last_attempt.finished:
            if body in ['1', '2', '3', '4']:
                if not last_answer:
                    new_answer = Answer.create(value=int(body), test_attempt=last_attempt, question_num=0)
                else:
                    new_answer = Answer.create(value=int(body), test_attempt=last_attempt,
                                               question_num=last_answer.question_num + 1)
                if (new_answer.question_num + 1) >= 10:
                    last_attempt.finished = True
                    last_attempt.save()
                    result = Answer.select(fn.SUM(Answer.value)).where(Answer.test_attempt == last_attempt).scalar()
                    if result in range(10, 18):
                        t = 0
                    elif result in range(18, 25):
                        t = 1
                    elif result in range(25, 31):
                        t = 2
                    elif result in range(31, 41):
                        t = 3
                    self.api.messages.send(peer_id=message['user_id'],
                                           attachment=self.image_mapping[TEXTS['results'][t]['img']])
                    self.api.messages.send(peer_id=message['user_id'], message=TEXTS['results'][t]['text'])
                else:
                    self.api.messages.send(peer_id=message['user_id'],
                                           message=TEXTS['test'][new_answer.question_num + 1])
            else:
                self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000000),
                                       message=TEXTS['test'][(last_answer.question_num + 1) if last_answer else 0])
        else:
            if body == 'тест':
                TestAttempt.create(user=user, finished=False)
                self.api.messages.send(peer_id=message['user_id'], message=TEXTS['test'][0])
            else:
                self.api.messages.send(peer_id=message['user_id'], random_id=random.randrange(1, 10000000),
                                       message=TEXTS['init'])
        return 'ok'
