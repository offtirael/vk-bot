import json

import cherrypy

from db import db, User, TestAttempt, Answer
from bot import Bot

TOKEN = '23b3b9504de63c661fdd087fbd8ad2b8e51dd65e7477e074289c993b4e643d5b7212b8a55431259b9be74'
GROUP_ID = 45980651
VERIFICATION_CODE = '77f22707'


class BotServer(object):
    def __init__(self, bot):
        self.bot = bot

    @cherrypy.expose
    def index(self):
        if 'content-length' in cherrypy.request.headers and \
                        'content-type' in cherrypy.request.headers and \
                        cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode("utf-8")
            data = json.loads(json_string)
            if data.get('type') == 'confirmation' and data.get('group_id') == GROUP_ID:
                return VERIFICATION_CODE
            elif data.get('type') == 'message_new':
                message = data.get('object')
                try:
                    result = self.bot.process_message(message)
                except Exception:
                    return 'ok'
                return result
            return 'ok'
        else:
            raise cherrypy.HTTPError(403)


if __name__ == '__main__':
    cherrypy.config.update({
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8080
    })

    db.connect()
    db.create_tables([User, TestAttempt, Answer], safe=True)

    cherrypy.quickstart(BotServer(Bot(TOKEN)), '/')
